# proxy2-wd

Fork of [proxy2](https://github.com/inaz2/proxy2) -- thanks to inaz2  
HTTP/HTTPS proxy in a single python script


## Features

Default proxy2 features:

* easy to customize
* require no external modules
* support both of IPv4 and IPv6
* support HTTP/1.1 Persistent Connection
* support dynamic certificate generation for HTTPS intercept

Customized features (i.e. what's changed in this fork):

* Remove cookies from request and Set-Cookie headers in response
* Removes 'X-Forwarded-For', 'Forwarded-For','Via','From', and 'Referer' headers from request
* Sets request User-Agent to a very common value 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36'
* Optional strict mode ``` --strict 1 ``` that obliterates Style and Script tags from the repsonse body


This script works on Python 2.7.
You need to install OpenSSL to intercept HTTPS connections.


## Usage

Just run as a script:

```
$ python proxy2.py
```

Above command runs the proxy on localhost:8080.
Verify it works by typing the below command on another terminal of the same host.

```
$ http_proxy=localhost:8080 curl http://www.example.com/
```

proxy2 is made for debugging/testing, so it only accepts connections from localhost.

To use another port, specify the port number with the ```--port``` flag.

```
$ python proxy2.py --port 3128
```
To enable strict mode use the ```--strict``` flag.

```
$ python proxy2.py --strict 1
```

## Enable HTTPS intercept

To intercept HTTPS connections, generate private keys and a private CA certificate:

```
$ ./setup_https_intercept.sh
```

Through the proxy, you can access http://proxy2.test/ and install the CA certificate in the browsers.


## Customization

You can easily customize the proxy and modify the requests/responses or save something to the files.
The ProxyRequestHandler class has 3 methods to customize:

* request_handler: called before accessing the upstream server
* response_handler: called before responding to the client
* save_handler: called after responding to the client with the exclusive lock, so you can safely write out to the terminal or the file system

This fork implements request and response sanitization measures, summarized above in the Features section.
